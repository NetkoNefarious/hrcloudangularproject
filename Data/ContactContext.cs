﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HRCloudAngularProject.Models;

namespace HRCloudAngularProject.Data
{
    public class ContactContext : DbContext
    {
        public ContactContext (DbContextOptions<ContactContext> options)
            : base(options)
        {
        }

        public DbSet<HRCloudAngularProject.Models.Contact> Contact { get; set; }
    }
}
