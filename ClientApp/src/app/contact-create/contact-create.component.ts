import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact/contact.service'
import { Contact } from 'src/app/interfaces/contact';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {

  contact: Contact = {
    name: '',
    surname: '',
    address: '',
  };

  constructor(private contactService: ContactService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  createContact(): void {
    const data = {
      name: this.contact.name,
      surname: this.contact.surname,
      address: this.contact.address,
    };

    this.contactService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.toastr.success("Submitted successfully")
          this.router.navigate(['/contacts'])
        },
        error => {
          console.log(error);
        });
  }

  resetForm(form: NgForm) {
    form.form.reset();
  }
}
