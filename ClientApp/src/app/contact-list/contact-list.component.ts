import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from '../interfaces/contact';
import { ContactService } from '../services/contact/contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  public contacts: Contact[];
  private router: Router;

  constructor(contactService: ContactService, router: Router) {
    this.router = router;

    contactService.readAll().subscribe(result => {
      this.contacts = result;
    }, error => console.error(error));
  }

  ngOnInit() {
  }

  createContact(): void {
    this.router.navigate(['/contacts/create']);
  }
}
