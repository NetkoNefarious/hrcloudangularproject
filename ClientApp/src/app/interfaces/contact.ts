export interface Contact {
  name: string;
  surname: string;
  address: string;
}
