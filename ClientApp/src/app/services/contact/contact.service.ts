import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from '../../interfaces/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  baseUrl: string;

  constructor(private httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl + "api/Contacts"
  }

  readAll(): Observable<any> {
    return this.httpClient.get<Contact[]>(this.baseUrl);
  }

  read(id): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}/${id}`);
  }

  create(data): Observable<any> {
    return this.httpClient.post(this.baseUrl, data);
  }

  update(id, data): Observable<any> {
    return this.httpClient.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.httpClient.delete(this.baseUrl);
  }

  searchByName(name): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}?name=${name}`);
  }
}
