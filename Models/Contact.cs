﻿using System.ComponentModel.DataAnnotations;

namespace HRCloudAngularProject.Models
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Surname { get; set; }
        [StringLength(50)]
        public string Address { get; set; }
    }
}
